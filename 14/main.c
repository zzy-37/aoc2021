#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#if 0
#define FILE_PATH "sample.txt"
#else
#define FILE_PATH "input.txt"
#endif

int main(void) {
    FILE *f = fopen(FILE_PATH, "r");
    if (!f) {
        perror("failed to open file");
        exit(1);
    }
    fseek(f, 0, SEEK_END);
    int fsize = ftell(f);
    printf("file size: %d\n", fsize);
    rewind(f);
    char buff[fsize];
    if (fread(buff, fsize, 1, f) < 0) {
        perror("failed to read file");
        exit(1);
    }
    fclose(f);

    char *fptr = buff;
    while (*fptr != '\n') fptr++;
    char *template = buff;
    size_t template_size = fptr - buff;
    printf("Template: %.*s\n", (int)template_size, template);

    assert(*++fptr == '\n');

#define RULES_CAP 128
    char rules[RULES_CAP][3];
    size_t rules_size = 0;

    while (++fptr + 2 < buff + fsize) {
        char a = *fptr++; assert_cap_char(a);
        char b = *fptr++; assert_cap_char(b);
        fptr += 4;
        char c = *fptr++; assert_cap_char(c);
        rules[rules_size][0] = a;
        rules[rules_size][1] = b;
        rules[rules_size][2] = c;
        ++rules_size;
        // printf("rule %zu: %c%c -> %c\n", rules_size, a, b, c);
    }

#define TABLE_CAP 'Z' - 'A' + 1
    typedef size_t Table[TABLE_CAP];
    Table t1[TABLE_CAP] = {0};
    Table t2[TABLE_CAP];
    Table *table = t1;
    Table *tmp = t2;
    for (int i = 0; i <= template_size - 2; ++i) {
        char a = template[i];
        char b = template[i + 1];
        ++table[a - 'A'][b - 'A'];
    }

    int step = 0;
    while (step++ < 40) {
        memset(tmp, 0, sizeof t1);
        for (int i = 0; i < rules_size; ++i) {
            int ia = rules[i][0] - 'A';
            int ib = rules[i][1] - 'A';
            int ic = rules[i][2] - 'A';
            size_t n = table[ia][ib];
            tmp[ia][ic] += n;
            tmp[ic][ib] += n;
        }

        Table *t = table;
        table = tmp;
        tmp = t;
    }

    size_t min = -1, max = 0;
    for (int i = 0; i < TABLE_CAP; ++i) {
        size_t n = 0;
        if ('A' + i == template[template_size - 1]) ++n;
        for (int j = 0; j < TABLE_CAP; ++j) n += table[i][j];
        if (n > 0) {
            min = (n < min) ? n : min;
            max = (n > max) ? n : max;
            // printf("%c: %zu\n", 'A' + i, n);
        }
    }
    printf("min: %zu, max: %zu\n", min, max);
    printf("part2: %zu\n", max - min);

    return 0;
}

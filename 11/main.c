#include <stdio.h>
#include <err.h>
#include <assert.h>

void update(int *g, int w, int h, int x, int y) {
    int i = x + y * w;
    if (x < 0 || x >= w || y < 0 || y >= h) return;
    if (++g[i] == 10) {
        for (int ox = -1; ox <= 1; ++ox) {
            for (int oy = -1; oy <= 1; ++oy) {
                update(g, w, h, x + ox, y + oy);
            }
        }
    }
}

int main(void) {
    FILE *f = fopen("input.txt", "r");
    if (!f) err(1, "failed to open file");

    while (fgetc(f) != '\n'); int width = ftell(f);
    fseek(f, 0, SEEK_END); int size = ftell(f);
    rewind(f); int height = (size + 1) / width--;

    int grid[width * height]; {
        rewind(f);
        int i = 0;
        for (char c; (c = fgetc(f)) != EOF;) {
            if (c != '\n') grid[i++] = c - '0';
        }
        assert(i == width * height);
    }

    fclose(f);

    int result = 0;
    int step = 0;
    while (step++ < 500) {
        for (int x = 0; x < width; ++x) {
            for (int y = 0; y < height; ++y) {
                update(grid, width, height, x, y);
            }
        }
        int n = 0;
        for (int x = 0; x < width; ++x) {
            for (int y = 0; y < height; ++y) {
                int i = x + y * height;
                if (grid[i] > 9) {
                    result++; grid[i] = 0;
                    if (++n == width * height) {
                        printf("part2: %d\n", step);
                        return 0;
                    }
                }
            }
        }
        if (step == 100) printf("part1: %d\n", result);
    }

    return 0;
}
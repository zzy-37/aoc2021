#include <stdio.h>
#if 0
#define FILEPATH "sample.txt"
#else
#define FILEPATH "input.txt"
#endif

#define BUFF_SIZE 100
int nums[BUFF_SIZE];
int nums_size;

#define BOARD_SIZE 5
typedef union {
    int square[BOARD_SIZE][BOARD_SIZE];
    int row[BOARD_SIZE*BOARD_SIZE];
} board;
#define BOARD_BUFF 100
board boards[BOARD_BUFF];
int boards_count;

void read_data() {
    FILE *f = fopen(FILEPATH, "r"); 
    nums_size = boards_count = 0;

    char c;
    while(fscanf(f, "%d%c", &nums[nums_size++], &c) > 0 && c != '\n');
    printf("read %d numbers.\n", nums_size);

    int i = 0;
    while(fscanf(f, "%d", &boards[boards_count].row[i++]) > 0)
        if (i == BOARD_SIZE*BOARD_SIZE) {
            i = 0;
            boards_count++;
        }
    printf("read %d boards\n", boards_count);

    fclose(f);
}

void dump_board(board b) {
    for (int i = 0; i < BOARD_SIZE; i++) {
        for (int j = 0; j < BOARD_SIZE; j++)
            printf("%3d", b.square[i][j]);
        printf("\n");
    }
    printf("\n");
}

void mark_board(board *b, int n) {
    for (int i = 0; i < BOARD_SIZE*BOARD_SIZE; i++)
        if (b->row[i] == n) {
            b->row[i] = ~n;
            break;
        }
}

int check_board(board b) {
    int j;
    for (int i = 0; i < BOARD_SIZE; i++) {
        j = 0;
        while(j < BOARD_SIZE && b.square[i][j] < 0) j++;
        if (j >= BOARD_SIZE) return 1;

        j = 0;
        while(j < BOARD_SIZE && b.square[j][i] < 0) j++;
        if (j >= BOARD_SIZE) return 1;
    }
    return 0;
}

int calc_sum(board b) {
    int sum = 0;
    for(int i = 0; i < BOARD_SIZE*BOARD_SIZE; i++)
        if (b.row[i] > 0) sum += b.row[i];
    return sum;
}

void part1() {
    read_data();
    printf("part1:\n");
    for(int i = 0; i < nums_size; i++) {
        for (int j = 0; j < boards_count; j++) {
            mark_board(&boards[j], nums[i]);
            if (check_board(boards[j])) {
                dump_board(boards[j]);
                printf("Board %d has won.\n", j);
                printf("Final score: %d\n", calc_sum(boards[j]) * nums[i]);
                return;
            }
        }
    }
}

void part2() {
    printf("part2:\n");
    read_data();
    int won_count = 0;
    for(int i = 0; i < nums_size; i++) {
        for (int j = 0; j < boards_count; j++) {
            if (!check_board(boards[j])) {
                mark_board(&boards[j], nums[i]);
                if (check_board(boards[j])) {
                    won_count++;
                    if (won_count >= boards_count) {
                        dump_board(boards[j]);
                        printf("Last board %d\n", j);
                        printf("Final score: %d\n", calc_sum(boards[j]) * nums[i]);
                        return;
                    }
                }
            }
        }
    }
}

int main() {
    part1();
    part2();

    return 0;
}

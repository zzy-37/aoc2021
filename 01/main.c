#include <stdlib.h>
#include <stdio.h>

void part1(FILE *data) {
    if (fseek(data, 0L, SEEK_SET) == -1) {
        fprintf(stderr, "Part2: failed to reset input stream to the beginning.\n");
        exit(1);
    }

    int conter = 0;
    int new, old = -1;
    while (fscanf(data, "%d", &new) > 0) {
        if (old >= 0 && new > old) conter++;
        old = new;
    }
    printf("part1: %d\n", conter);
}

#define NUMCAP 2048
void part2(FILE *data) {
    if (fseek(data, 0L, SEEK_SET) == -1) {
        fprintf(stderr, "Part2: failed to reset input stream to the beginning.\n");
        exit(1);
    }

    int numlist[NUMCAP];
    int conter = 0;
    for (int i = 0; i < NUMCAP && fscanf(data, "%d", numlist+i) > 0; i++) {
        if (i - 3 >= 0 && numlist[i] > numlist[i-3]) conter++;
    }
    printf("part2: %d\n", conter);
}

int main(int argc, char **argv) {
    char *prog = argv[0];
    if (argc < 2) {
        fprintf(stderr, "%s: No input file.\n", prog);
        printf("Usage: %s <inpur file>\n", prog);
        exit(1);
    }

    char *inputfile = argv[1];
    FILE *fd = fopen(inputfile, "r");
    if (fd == NULL) {
        fprintf(stderr, "%s: Could not open file '%s'.\n", prog, inputfile);
        exit(1);
    }

    part1(fd);
    part2(fd);

    return 0;
}


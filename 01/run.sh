#!/bin/sh

set -x
prg=main
cc -o $prg main.c && ./$prg ${1:-input.txt}

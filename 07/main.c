#include <stdio.h>
#include <stdlib.h>

#if 0
#define FILEPATH "sample.txt"
#else
#define FILEPATH "input.txt"
#endif

#define max(a, b) ((a > b) ? a : b)
#define min(a, b) ((a > b) ? b : a)

#define BUFF 1024
int crabs[BUFF];
int crab_size = 0;
int max_pos = 0;

void read_data() {
    FILE *f = fopen(FILEPATH, "r");

    int *p = crabs;
    while(fscanf(f, "%d,", p) > 0) {
        max_pos = max(max_pos, *p);
        p++;
        crab_size++;
    }
    printf("read %d crabs\n", crab_size);
    printf("max_pos: %d\n", max_pos);

    fclose(f);
}

int align_pos1(int pos) {
    int fuel = 0;
    for (int i = 0; i < crab_size; i++)
        fuel += abs(crabs[i] - pos);
    return fuel;
}

int align_pos2(int pos) {
    int fuel = 0;
    for (int i = 0; i < crab_size; i++) {
        int n = abs(crabs[i] - pos);
        fuel += (1 + n) * n / 2;
    }
    return fuel;
}

void part1() {
    int min_fuel = align_pos1(0);
    for (int i = 1; i <= max_pos; i++)
        min_fuel = min(min_fuel, align_pos1(i));
    printf("part1: %d\n", min_fuel);
}

void part2() {
    int min_fuel = align_pos2(0);
    for (int i = 1; i <= max_pos; i++)
        min_fuel = min(min_fuel, align_pos2(i));
    printf("part2: %d\n", min_fuel);
}

int main() {
    read_data();

    part1();
    part2();

    return 0;
}

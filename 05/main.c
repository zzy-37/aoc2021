#include <stdio.h>
#if 0
#define FILEPATH "sample.txt"
#define LINE_BUF 10
#define BOARD_SIZE 10
#else
#define FILEPATH "input.txt"
#define LINE_BUF 500
#define BOARD_SIZE 1000
#endif

#define max(a, b) ((a > b) ? a : b)
#define min(a, b) ((a < b) ? a : b)
#define diff(a, b) ((a > b) ? a-b : b-a)

enum {x1, y1, x2, y2};
typedef int line[4];
line lines[LINE_BUF];
int line_count = 0;

int board[BOARD_SIZE][BOARD_SIZE] = {0};

void read_data() {
    FILE *f = fopen(FILEPATH, "r");

    line *l = lines;
    while(fscanf(f, "%d,%d -> %d,%d\n", *l+x1, *l+y1, *l+x2, *l+y2) > 0) {
        line_count++;
        l++;
    }
    printf("read %d lines\n", line_count);

    fclose(f);
}

void print_line(line l) {
    printf("%d,%d -> %d,%d\n", l[x1], l[y1], l[x2], l[y2]);
}

void mark_hv(line l) {
    if (l[x1] == l[x2]) {
        for (int i = min(l[y1], l[y2]); i <= max(l[y1], l[y2]); i++)
            board[l[x1]][i]++;
    } else if (l[y1] == l[y2]) {
        for (int i = min(l[x1], l[x2]); i <= max(l[x1], l[x2]); i++)
            board[i][l[y1]]++;
    }
}

void mark_dia(line l) {
    if (diff(l[x1], l[x2]) == diff(l[y1], l[y2])) {
        int xstep = (l[x1] < l[x2]) ? 1 : -1;
        int ystep = (l[y1] < l[y2]) ? 1 : -1;

        int x = l[x1], y = l[y1];
        board[x][y]++;
        while (x != l[x2] && y != l[y2]) {
            x += xstep;
            y += ystep;
            board[x][y]++;
        }
    }
}

void dump_board() {
    for(int i = 0; i < BOARD_SIZE; i++) {
        for(int j = 0; j < BOARD_SIZE; j++) {
            if (board[j][i] == 0)
                printf(" .");
            else
                printf("%2d" , board[j][i]);
        }
        printf("\n");
    }
}

void part1() {
    for(int i = 0; i < line_count; i++) {
        mark_hv(lines[i]);
    }

    /* dump_board(); */

    int count = 0;
    for(int i = 0; i < BOARD_SIZE; i++)
        for(int j = 0; j < BOARD_SIZE; j++)
            if (board[i][j] >= 2) count++;
    printf("part1: %d\n", count);
}

void part2() {
    for(int i = 0; i < line_count; i++) {
        mark_dia(lines[i]);
    }

    /* dump_board(); */

    int count = 0;
    for(int i = 0; i < BOARD_SIZE; i++)
        for(int j = 0; j < BOARD_SIZE; j++)
            if (board[i][j] >= 2) count++;
    printf("part2: %d\n", count);
}

int main() {
    read_data();

    part1();
    part2();

    return 0;
}

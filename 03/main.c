#include <stdio.h>
#include <stdlib.h>

#if 0
#define FILEPATH "sample.txt"
#define ACC_SIZE 5
#define BITMASK 037
#else
#define FILEPATH "input.txt"
#define ACC_SIZE 12
#define BITMASK 07777
#endif

void part1(FILE *f) {
    int i = 0, c;
    int acc[ACC_SIZE] = {0};
    while ((c = fgetc(f)) != EOF) {
        switch(c) {
            case '\n': i = 0; break;
            case '1': acc[i++]++; break;
            case '0': acc[i++]--; break;
            default: printf("unreachable\n"); exit(1);
        }
    }

    int gam = 0;
    printf("part1:\n");
    printf("ACCUMULATOR(size=%d):", ACC_SIZE);
    for (int i = 0; i < ACC_SIZE; i++) {
        printf(" %d", acc[i]);
        gam <<= 1;
        if (acc[i] > 0) gam++;
    }
    int eps= ~gam & BITMASK;
    printf("\ngam: %d, eps: %u, power: %d\n", gam, eps, gam*eps);
}

#define BUFF_SIZE 1024
int report[BUFF_SIZE] = {0};
int report_size = 0;
enum type { OXY, CO2 };

int acc_digit(int digit, int index[], int index_size, enum type gas) {
    if (index_size == 1) return index[0];

    int one_index[index_size];
    int zero_index[index_size];
    int one_size = 0, zero_size = 0;
    for (int i = 0; i < index_size; i++) {
        if (index[i] & 1 << digit-1) one_index[one_size++] = index[i];
        else zero_index[zero_size++] = index[i];
    }

    if (gas == OXY) {
        if (one_size >= zero_size)
            return acc_digit(digit-1, one_index, one_size, OXY);
        else
            return acc_digit(digit-1, zero_index, zero_size, OXY);
    } else {
        if (one_size < zero_size)
            return acc_digit(digit-1, one_index, one_size, CO2);
        else
            return acc_digit(digit-1, zero_index, zero_size, CO2);
    }
}

void part2(FILE *data) {
    int c;
    while ((c = fgetc(data)) != EOF) {
        if (c == '\n') report_size++;
        else {
            report[report_size] <<= 1;
            if (c == '1') report[report_size]++;
        }
    }

    int oxy = acc_digit(ACC_SIZE, report, report_size, OXY);
    int co2 = acc_digit(ACC_SIZE, report, report_size, CO2);
    printf("part2: oxy=%d, co2=%d, mul=%d\n", oxy, co2,  oxy*co2);
}

int main() {
    FILE *f = fopen(FILEPATH, "r");

    part1(f);
    if(fseek(f, 0, SEEK_SET) == -1) {
        fprintf(stderr, "fseek: failed to reset file stream to the beginning.\n");
        exit(1);
    }
    part2(f);

    fclose(f);
    return 0;
}

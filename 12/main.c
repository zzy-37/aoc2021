#include <stdio.h>
#include <assert.h>
#include <err.h>

#define SV_INPLEMENTATION
#include "sv.h"

#define INPUT_FILE "input.txt"
#define PART2

#define CAVE_CAP 32
struct {
    char buff[512];
    String_View caves[CAVE_CAP];
    size_t cave_sz;
    int graph[CAVE_CAP][CAVE_CAP];
    size_t graph_sz[CAVE_CAP];
} cave_data = {0};

int get_cave_index(String_View c) {
    for (int i = 0; i < cave_data.cave_sz; ++i) {
        if (sv_equal(c, cave_data.caves[i])) return i;
    }
    return -1;
}

void read_cave_graph_from_file(const char* path) {
    FILE *f = fopen(path, "r");
    if (!f) err(1, "failed to open file");
    int read_size = fread(cave_data.buff, 1, sizeof cave_data.buff, f);
    if (read_size < 0) err(1, "fread failed");
    fclose(f);

    printf("read %d bytes\n", read_size);

    String_View s = { cave_data.buff, read_size };
    while (s.size > 0) {
        String_View line = sv_chop_by_delimiter(&s, SV("\n"));
        String_View a = sv_chop_by_delimiter(&line, SV("-")), b = line;

        int ia = get_cave_index(a);
        if (ia < 0) {
            ia = cave_data.cave_sz;
            cave_data.caves[cave_data.cave_sz++] = a;
        }
        int ib = get_cave_index(b);
        if (ib < 0) {
            ib = cave_data.cave_sz;
            cave_data.caves[cave_data.cave_sz++] = b;
        }
        assert(cave_data.cave_sz < CAVE_CAP);

        cave_data.graph[ia][cave_data.graph_sz[ia]++] = ib;
        cave_data.graph[ib][cave_data.graph_sz[ib]++] = ia;
    }
}

int main(void) {
    read_cave_graph_from_file(INPUT_FILE);

    // Print cave graph
    printf("cave size: %zu\n", cave_data.cave_sz);
    for (int i = 0; i < cave_data.cave_sz; ++i) {
        printf(SV_FMT":", SV_ARG(cave_data.caves[i]));
        for (int j = 0; j < cave_data.graph_sz[i]; ++j) {
            printf(" "SV_FMT, SV_ARG(cave_data.caves[cave_data.graph[i][j]]));
        }
        printf("\n");
    }

    int istart = get_cave_index(SV("start"));
    int iend = get_cave_index(SV("end"));
    printf("s: %d, e: %d\n", istart, iend);

    int valid_path = 0;
    struct path {
        int route[CAVE_CAP];
        size_t len;
        bool twice;
    };
#define path_append(path, a) path.route[path.len++] = (a)

#define STACK_CAP 128
    struct path stack[STACK_CAP];
    int stack_sz = 0;

    struct path start = {0};
    path_append(start, istart);
    stack[stack_sz++] = start;

    int count = 0;
    while (stack_sz > 0) {
        struct path current = stack[--stack_sz];
        int tail = current.route[current.len - 1];

        if (tail == iend) {
            // for (int i = 0; i < current.len; ++i) {
            //     printf(" "SV_FMT, SV_ARG(cave_data.caves[current.route[i]]));
            // } printf("\n");
            valid_path++; continue;
        }

        for (int i = 0; i < cave_data.graph_sz[tail]; ++i) {
            int nexti = cave_data.graph[tail][i];
            char firstc = cave_data.caves[nexti].data[0];
            struct path new = current;
            if (firstc >= 'a' && firstc <= 'z') {
                bool visited = false;
                for (int j = 0; j < current.len; ++j) {
                    if (current.route[j] == nexti) {
                        visited = true; break;
                    }
                }
#ifdef PART2
                if (visited) {
                    if (current.twice || nexti == istart) continue;
                    else new.twice = true;
                }
#else
                if (visited) continue;
#endif
            }
            path_append(new, nexti);
            stack[stack_sz++] = new;
            assert(stack_sz < STACK_CAP);
        }
    }
#ifdef PART2
    printf("part2: %d\n", valid_path);
#else
    printf("part1: %d\n", valid_path);
#endif

    return 0;
}
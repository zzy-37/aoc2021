#ifndef SV_H
#define SV_H

#include <stdbool.h>
#include <string.h>

typedef struct {
    char *data;
    size_t size;
} String_View;

#define SV_FMT "%.*s"
#define SV_ARG(sv) (int)sv.size, sv.data

#define SV sv_from_str_literal
#define sv_from_str_literal(str) ({ char _str[] = (str); (String_View){ _str, sizeof _str - 1 }; })

bool sv_equal(String_View a, String_View b);
String_View sv_chop_by_delimiter(String_View *s, String_View delim);
void sv_append(String_View *s, String_View a);

#endif

#ifdef SV_INPLEMENTATION

bool sv_equal(String_View a, String_View b) {
    if (a.size != b.size) return false;
    for (int i = 0; i < a.size; ++i) {
        if (a.data[i] != b.data[i]) return false;
    }
    return true;
}

String_View sv_chop_by_delimiter(String_View *s, String_View delim) {
    String_View result = *s;
    for (int i = 0; i < s->size - delim.size; ++i) {
        if (sv_equal(delim, (String_View){ s->data + i, delim.size })) {
            result.size = i;
            s->data += i + delim.size;
            s->size -= i + delim.size;
            return result;
        }
    }
    s->size = 0;
    s->data = NULL;
    return result;
}

void sv_append(String_View *s, String_View a) {
    memcpy(s->data + s->size, a.data, a.size);
    s->size += a.size;
}

#endif
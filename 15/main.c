#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>
#include <string.h>

#define PART2
#if 0
#define FILE_PATH "sample.txt"
#else
#define FILE_PATH "input.txt"
#endif

int main(void) {
    FILE *f = fopen(FILE_PATH, "r");
    if (!f) {
        perror("failed to open file");
        exit(1);
    }
    fseek(f, 0, SEEK_END);
    int fsize = ftell(f);
    printf("file size: %d\n", fsize);
    rewind(f);
    char buff[fsize];
    if (fread(buff, fsize, 1, f) < 0) {
        perror("failed to read file");
        exit(1);
    }
    fclose(f);

    char *fptr = buff;
    while (*fptr++ != '\n');
    int width = fptr - buff;
    int height = (fsize + 1) / width--;
    printf("w: %d, h: %d\n", width, height);

    int board[width][height];
    for (int y = 0; y < height; ++y) {
        for (int x = 0; x < width; ++x) {
            board[x][y] = buff[x + y * (width + 1)] - '0';
            // printf("%d", board[x][y]);
        }
        // printf("\n");
    }

#ifdef PART2
    int w = width * 5, h = height * 5;
#define board_risk(x, y) ((board[x % width][y % height] + x / width + y / height - 1) % 9 + 1)
#else
    int w = width, h = height;
#define board_risk(x, y) (board[x][y])
#endif

    unsigned min_risk[w][h];
    memset(min_risk, -1, sizeof(min_risk));
    min_risk[0][0] = 0;

#define STACK_CAP 1024 * 200
    int stack[STACK_CAP][2];
    size_t stack_size = 0;
#define stack_push(x, y) do {         \
    stack[stack_size][0] = x;         \
    stack[stack_size][1] = y;         \
    assert(++stack_size < STACK_CAP); \
} while (0)

    stack_push(0, 0);

    int offsets[2][4] = {
        {-1, 0, 1, 0},
        {0, -1, 0, 1}
    };

    while (stack_size-- > 0) {
        int cx = stack[stack_size][0];
        int cy = stack[stack_size][1];
        for (int i = 0; i < 4; ++i) {
            int x = cx + offsets[0][i],
                y = cy + offsets[1][i];
            if (x < 0 || x >= w || y < 0 || y >= h) continue;
            unsigned r = min_risk[cx][cy] + board_risk(x, y);
            if (r >= min_risk[x][y] || r >= min_risk[w - 1][h - 1]) continue;
            min_risk[x][y] = r;
            stack_push(x, y);
            // printf("x: %d, y: %d, r: %u\n", x, y, r);
        }
        // printf("stack size: %zu\n", stack_size);
    }

#ifdef PART2
    printf("part2: %u\n", min_risk[w - 1][h - 1]);
#else
    printf("part1: %u\n", min_risk[w - 1][h - 1]);
#endif

    return 0;
}

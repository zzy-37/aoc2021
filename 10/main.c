#include <stdio.h>
#include <err.h>
#include <assert.h>

char process_line(char *line, unsigned long *comp_score) {
    char unclose[128];
    int unclose_sz = 0;

    char c;
    while ((c = *line++) && c != '\n') {
        if (c == '(' || c == '[' || c == '{' || c == '<')
            unclose[unclose_sz++] = c;
        else if ((c == ')' && unclose[unclose_sz - 1] == '(')
              || (c == ']' && unclose[unclose_sz - 1] == '[')
              || (c == '}' && unclose[unclose_sz - 1] == '{')
              || (c == '>' && unclose[unclose_sz - 1] == '<')) {
            if (--unclose_sz < 0) return c;
        } else return c;
    }

    // part 2
    // calculate complete string score
    unsigned long score = 0;
    for (int i = unclose_sz - 1; i >= 0; --i) {
        score *= 5;
        switch (unclose[i]) {
        case '(': score += 1; break;
        case '[': score += 2; break;
        case '{': score += 3; break;
        case '<': score += 4; break;
        // unreachable
        default: assert(0); break;
        }
    }
    *comp_score = score;

    return -1;
}

int main(void) {
    char filepath[] = "input.txt";
    FILE *f = fopen(filepath, "r");
    if (f == NULL) err(1, "failed to open file '%s'", filepath);

    char buf[512];
    int error_score = 0;
    unsigned long comp_scores[512] = {0};
    int comp_score_sz = 0;
    while (fgets(buf, sizeof buf, f)) {
        unsigned long comp_score;
        switch (process_line(buf, &comp_score)) {
        case ')': error_score += 3; break;
        case ']': error_score += 57; break;
        case '}': error_score += 1197; break;
        case '>': error_score += 25137; break;
        case -1 :
            comp_score_sz++;
            for (int i = 0; i < comp_score_sz; ++i) {
                if (comp_score > comp_scores[i]) {
                    for (int j = comp_score_sz; j > i; --j)
                        comp_scores[j] = comp_scores[j - 1];
                    comp_scores[i] = comp_score;
                    break;
                }
            }
            break;
        // unreachable
        default: assert(0); break;
        }
    }
    printf("part1: %d\n", error_score);
    printf("part2: %lu\n", comp_scores[comp_score_sz / 2]);

    fclose(f);
    return 0;
}
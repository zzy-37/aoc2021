#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#if 0
#define FILEPATH "sample.txt"
#else
#define FILEPATH "input.txt"
#endif

void part1(FILE *data) {
    char buf[20];
    int x;
    int pos = 0, depth = 0;
    while (fscanf(data, "%s %d", buf, &x) > 0) {
        switch(*buf) {
            case 'f':
                pos += x;
                break;
            case 'u':
                depth -= x;
                break;
            case 'd':
                depth += x;
                break;
        }
    }
    printf("part1: %d\n", pos*depth);
}

void part2(FILE *data) {
    char buf[20];
    int x;
    int pos = 0, depth = 0, aim = 0;
    while (fscanf(data, "%s %d", buf, &x) > 0) {
        switch(*buf) {
            case 'f':
                pos += x;
                depth += aim*x;
                break;
            case 'u':
                aim -= x;
                break;
            case 'd':
                aim += x;
                break;
        }
    }
    printf("part2: %d\n", pos*depth);
}

int main() {
    FILE *f = fopen(FILEPATH, "r");
    if (f == NULL) {
        perror(FILEPATH);
        exit(1);
    }

    part1(f);

    // reset input stream
    if (fseek(data, 0, SEEK_SET) == -1) {
        perror("part2: failed to reset input stream to the beginning");
        exit(1);
    }

    part2(f);

    fclose(f);
    return 0;
}


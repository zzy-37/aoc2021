#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <err.h>
#include <assert.h>

typedef struct {
    size_t width, height;
    char *data;
} Board, Pixels;

typedef struct {
    unsigned char r, g, b;
} color;

typedef enum {
    false,
    true
} bool;

Board read_file_to_board(char *filepath) {
    FILE *f = fopen(filepath, "r");
    if (f == NULL) err(1, "failed to open file");

    char buffer[512];
    int line_count = 0;
    int line_length;
    while (fgets(buffer, sizeof buffer, f)) {
        int len = strlen(buffer);
        if (buffer[len - 1] == '\n') len -= 1;
        if (line_count == 0) line_length = len;
        else assert(len == line_length);
        line_count++;
    }

    Board b;
    b.width = line_length;
    b.height = line_count;
    b.data = malloc(line_length * line_count);

    rewind(f);

    for (int i = 0; i < line_count; ++i) {
        fgets(buffer, sizeof buffer, f);
        memcpy(&b.data[i * line_length], buffer, line_length);
    }

    fclose(f);

    return b;
}

int board_get_value(Board b, int x, int y) {
    if (x < 0 || x >= b.width || y < 0 || y >= b.height) return -1;
    int value = b.data[x + y * b.width] - '0';
    assert(value >= 0 && value <= 9);
    return value;
}

int board_index_of_point(Board b, int x, int y) {
    if (x < 0 || x >= b.width || y < 0 || y >= b.height) return -1;
    return x + y * b.width;
}

bool board_is_low_point(Board b, int x, int y) {
    int current = board_get_value(b, x, y);
    assert(current != -1);

    int xs[4] = { x + 1, x - 1, x, x };
    int ys[4] = { y, y, y + 1, y - 1 };
    for (int i = 0; i < 4; ++i) {
        int val = board_get_value(b, xs[i], ys[i]);
        if (val == -1) continue;
        if (val <= current) return false;
    }
    return true;
}


#define sizeof_pixel_data(p) (p.width * p.height * 3)

Pixels initialize_pixels(size_t width, size_t height) {
    return (Pixels) {
        .width  = width,
        .height = height,
        .data = malloc(3 * width * height),
    };
}

color color_from_u32 (unsigned int c) {
    assert(c <= 0xFFFFFF);
    return (color) {
        .r = c >> 2 * 8 & 0xFF,
        .g = c >> 1 * 8 & 0xFF,
        .b = c          & 0xFF,
    };
}

void pixels_color_pixel(Pixels p, int x, int y, color c) {
    if (x < 0 || x >= p.width || y < 0 || y >= p.height) return;

    int i = (x + y * p.width) * 3;
    p.data[i + 0] = c.r;  // r
    p.data[i + 1] = c.g;  // g
    p.data[i + 2] = c.b;  // b
}

void pixels_write_ppm(char *filepath, Pixels p) {
    FILE *output = fopen(filepath, "w");
    fprintf(output, "P6 %ld %ld 255\n", p.width, p.height);
    fwrite(p.data, sizeof_pixel_data(p), 1, output);
    fclose(output);
}

void ppm_test() {
    int scale = 3;
    int width = 200, height = 100;
    Pixels p = initialize_pixels(width * scale, height * scale);

    color c;
    c.g = 0;
    for (int x = 0; x < p.width; ++x) {
        for (int y = 0; y < p.height; ++y) {
            c.r = (int) ((float) x / p.width  * 255);
            c.b = (int) ((float) y / p.height * 255);
            pixels_color_pixel(p, x, y, c);
        }
    }

    char *outpath = "output.ppm";
    pixels_write_ppm(outpath, p);

    free(p.data);
}

int board_basin_size(Board b, int x, int y, Pixels p) {
    typedef size_t index;

    index unchecked[1024];
    size_t unchecked_sz = 0;

#define INDEX_GET_X(idx) (idx % b.width)
#define INDEX_GET_Y(idx) (idx / b.width)

    // push the index of the low point to uncheck
    unchecked[unchecked_sz++] = board_index_of_point(b, x, y);

    int size = 0;
    for (int k = 0; k < unchecked_sz; ++k) {
        index idx = unchecked[k];

        int cx = INDEX_GET_X(idx);
        int cy = INDEX_GET_Y(idx);
        int value = board_get_value(b, cx, cy);

        if (value == -1 || value == 9) continue;

        size++;

        // get 4 points around current point
        int xs[4] = { cx + 1, cx - 1, cx, cx };
        int ys[4] = { cy, cy, cy + 1, cy - 1 };

        for (int j = 0; j < 4; ++j) {
            int p_idx = board_index_of_point(b, xs[j], ys[j]);
            if (p_idx == -1) continue;

            bool added = false;
            for (int i = 0; i < unchecked_sz; ++i) {
                if (p_idx == unchecked[i]) {
                    added = true;
                    break;
                }
            }
            if (!added) unchecked[unchecked_sz++] = p_idx;
        }
    }

    return size;
}

int main(void) {
    char filepath[] = "input.txt";

    Board b = read_file_to_board(filepath);

// part 1
{
    int sum = 0;
    for (int y = 0; y < b.height; ++y) {
        for (int x = 0; x < b.width; ++x) {
            if (board_is_low_point(b, x, y)) sum += board_get_value(b, x, y) + 1;
        }
    }
    printf("part1: %d\n", sum);
}

//     // visualize board
//     int scale = 10;
//     Pixels p = initialize_pixels(b.width * scale, b.height * scale);
// {
//     for (int y = 0; y < p.height; ++y) {
//         for (int x = 0; x < p.width; ++x) {
//             int val = board_get_value(b, x / scale, y / scale);
//             int bri = (int)((float) val / 9 * 255);
//             pixels_color_pixel(p, x, y, (color){bri, bri, bri});
//         }
//     }
// }
//     pixels_write_ppm("output.ppm", p);
//     free(p.data);

// part2
{
    int max_size[3] = {0};

    for (int x = 0; x < b.width; ++x) {
        for (int y = 0; y < b.height; ++y) {
            if (board_is_low_point(b, x, y)) {
                int size = board_basin_size(b, x, y);
                for (int i = 0; i < 3; ++i) {
                    if (size > max_size[i]) {
                        for (int j = 3 - 1; j > i ; --j) {
                            max_size[j] = max_size[j - 1];
                        }
                        max_size[i] = size;
                        break;
                    }
                }
            }
        }
    }

    int result = 1;
    for (int i = 0; i < sizeof max_size / sizeof max_size[0]; ++i) {
        result *= max_size[i];
    }
    printf("part2: %d\n", result);
}

    free(b.data);
    return 0;
}
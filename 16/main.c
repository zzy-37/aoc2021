#include <stdio.h>
#include <stdlib.h>

#define ERROR(...) do { fprintf(stderr, __VA_ARGS__); exit(1); } while (0)

int parse_bin(char *bits, int len) {
    int n = 0;
    for (int i = 0; i < len; ++i) {
        if (bits[i] == '1') n |= 1 << len - 1 - i;
        else if (bits[i] != '0') ERROR("Invalid character '%c' while parsing binary number\n", bits[i]);
    }

    return n;
}

int parse_bin_and_advance(char **bits, int len) {
    int n = parse_bin(*bits, len);
    *bits += len;
    return n;
}

int parse_packet(char **b) {
    int version = parse_bin_and_advance(b, 3);
    int type = parse_bin_and_advance(b, 3);

    if (type == 4) {
        int parsed_num = 0;
        for (;;) {
            char prefix = *(*b)++;
            int n = parse_bin_and_advance(b, 4);
            parsed_num = parsed_num << 4 | n;
            if (prefix == '0') break;
            else if (prefix != '1') ERROR("Invalid prefix '%c' while parsing number literal\n", prefix);
        }
        printf("literal value: %d\n", parsed_num);
    } else {
        char length_type = *(*b)++;
        printf("sub-pack:\n");
        switch (length_type) {
            case '0':
                int packet_len = parse_bin_and_advance(b, 15);
                char *start = *b;
                while (*b - start < packet_len) {
                    printf("    ");
                    version += parse_packet(b);
                }
                break;
            case '1':
                int packet_num = parse_bin_and_advance(b, 11);
                while (packet_num-- > 0) {
                    printf("    ");
                    version += parse_packet(b);
                }
                break;
            default: ERROR("Invalid length type ID '%c'\n", length_type);
        }
    }

    return version;
}

char *get_hex_bin(char c) {
    static const char table[16][4] = {
        "0000", "0001", "0010", "0011",
        "0100", "0101", "0110", "0111",
        "1000", "1001", "1010", "1011",
        "1100", "1101", "1110", "1111",
    };

         if (c >= '0' && c <= '9') return table[c - '0'];
    else if (c >= 'A' && c <= 'F') return table[c - 'A' + 10];
    else ERROR("Invalid hex digit '%c'\n", c);
}

int main(void) {
    char bits1[] = "110100101111111000101000";
    char *b = bits1;
    int ver = parse_packet(&b);
    printf("ver: %d\n", ver);

    char bits2[] = "00111000000000000110111101000101001010010001001000000000";
    b = bits2;
    printf("ver2: %d\n", parse_packet(&b));

    char bits3[] = "11101110000000001101010000001100100000100011000001100000";
    b = bits3;
    printf("ver3: %d\n", parse_packet(&b));

    char bin[4];
    char *h = get_hex_bin('A');
    printf("hex %c: ", 'A');
    for (int i = 0; i < 4; ++i) {
        bin[i] = h[i];
        printf("%c", h[i]);
    }
    printf("\n");

    return 0;
}

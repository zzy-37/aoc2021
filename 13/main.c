#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int main(void) {
    FILE *f = fopen("input.txt", "r");
    if (!f) {
        perror("failed to open file");
        exit(1);
    }
    fseek(f, 0, SEEK_END);
    int fsize = ftell(f);
    // printf("buff size: %d\n", fsize);
    rewind(f);
    char buff[fsize];
    if (fread(buff, fsize, 1, f) < 0) {
        perror("failed to read file");
        exit(1);
    }
    fclose(f);

    union point {
        struct { int x, y; };
        int i[2];
    };
    union point points[1024];
    int point_size = 0;

    char *ptr = buff;
    while (*ptr != '\n') {
        char *c1 = ptr;
        while (*ptr++ != ',');
        char *c2 = ptr;
        while (*ptr++ != '\n');
        int i1 = atoi(c1);
        int i2 = atoi(c2);
        points[point_size++] = (union point){ i1, i2 };
        // printf("x: %d, y: %d\n", i1, i2);
    }
    ++ptr;

    bool first = true;
    enum fold { horiz, vert };
    while (ptr < buff + fsize) {
        while (*ptr++ != ' ');
        while (*ptr++ != ' ');
        char* pos = ptr;

        enum fold f;
        if (*pos == 'x') { f = horiz; }
        else if (*pos == 'y') { f = vert; }
        else {
            fprintf(stderr, "unreachable\n");
            return 1;
        }
        pos += 2;
        int fold_line = atoi(pos);

        int pt_num = 0;
        for (int i = 0; i < point_size; ++i) {
            union point folded = points[i];
            if (points[i].i[f] > fold_line) {
                folded.i[f] = fold_line - (folded.i[f] - fold_line);
            }
            bool overlap = false;
            for (int j = 0; j < i; ++j) {
                if (folded.x == points[j].x && folded.y == points[j].y) {
                    overlap = true;
                    break;
                }
            }
            if (overlap) continue;
            points[pt_num++] = folded;
        }
        point_size = pt_num;
        if (first) {
            first = false;
            printf("part1: %d\n", pt_num);
        }
        // printf("%d points remaining\n", pt_num);

        while (*ptr++ != '\n' && ptr < buff + fsize);
        // printf("f: %.*s\n", (int)(ptr - pos - 1), pos);
    }

    int maxx = 0;
    int maxy = 0;
    for (int i = 0; i < point_size; ++i) {
        maxx = (points[i].x > maxx) ? points[i].x : maxx;
        maxy = (points[i].y > maxy) ? points[i].y : maxy;
        // printf("[%2d, %2d]\n", points[i].x, points[i].y);
    }
    ++maxx; ++maxy;
    // printf("maxx: %d, maxy: %d\n", maxx, maxy);

    bool grid[maxx * maxy];
    for (int i = 0; i < maxx * maxy; ++i) grid[i] = false;

    for (int i = 0; i < point_size; ++i) {
        int x = points[i].x;
        int y = points[i].y;
        grid[x + y * maxx] = true;
    }

    printf("part2:\n");
    for (int y = 0; y < maxy; ++y) {
        for (int x = 0; x < maxx; ++x) {
            if (grid[x + y * maxx]) printf("+");
            else printf(" ");
        }
        printf("\n");
    }

    return 0;
}

#include <stdio.h>
#include <err.h>
#include <assert.h>

#define print_bit_pattern(item) _print_bit_pattern((char *) &item, sizeof item)
void _print_bit_pattern(char *ptr, size_t size) {
    char buf[size * 9];
    int j = 0;
    while(j < sizeof buf) {
        char c = *ptr++;
        for (int i = 0; i < 8; ++i)
            buf[j++] = (c & 1 << 7 - i) ? '1' : '0';
        buf[j++] = ' ';
    }
    buf[sizeof buf - 1] = 0; // null terminate the string
    printf("%s\n", buf);
}

char parse_digit(char **line, int *len) {
    // buffer for storing seven segment display
    size_t size = 0;
    char *buf = *line;

    while (size++ < 8) {
        char c = buf[size];
        if (c == ' ' || c == '\n') break;
    }

    // the maximum possible length of buf is 7
    assert(size > 0 && size <= 7);

    if (len) *len = size;

    char result = 0;

    for (int i = 0; i < size; ++i)
        result |= 1 << buf[i] - 'a';

    *line += size + 1;

    return result;
}

int parse_line(char *line) {
    char digits[10] = {0};

    // 10 digits
    {
        char five[3]; // 2, 3, 5
        char six[3];  // 0, 6, 9
        int five_sz = 0;
        int six_sz = 0;
        for (int i = 0; i < 10; ++i) {
            int len;
            char encoded = parse_digit(&line, &len);

            switch (len) {
                case 2: digits[1] = encoded; break;
                case 4: digits[4] = encoded; break;
                case 3: digits[7] = encoded; break;
                case 7: digits[8] = encoded; break;

                case 5: five[five_sz++] = encoded; break;
                case 6: six[six_sz++] = encoded; break;

                default: assert(0); break;
            }
        }

        assert(five_sz == 3 && six_sz == 3);

        for (int i = 0; i < five_sz; ++i) {
            if ((five[i] & digits[1]) == digits[1]) {
                digits[3] = five[i];
                five[i] = five[--five_sz];
                break;
            }
        }
        assert(digits[3] != 0 && five_sz == 2);

        for (int i = 0; i < six_sz; ++i) {
            if ((six[i] & digits[3]) == digits[3]) {
                digits[9] = six[i];
                six[i] = six[--six_sz];
                break;
            }
        }
        assert(digits[9] != 0 && six_sz == 2);

        if ((five[0] & digits[9]) == five[0]) {
            digits[5] = five[0];
            digits[2] = five[1];
        } else {
            digits[5] = five[1];
            digits[2] = five[0];
        }

        if ((six[0] & digits[5]) == digits[5]) {
            digits[6] = six[0];
            digits[0] = six[1];
        } else {
            digits[6] = six[1];
            digits[0] = six[0];
        }
    }

    // delimiter
    assert(*line++ == '|' && *line++ == ' ');

    // 4 digits
    int val = 0;
    for (int i = 0; i < 4; ++i) {
        char encoded = parse_digit(&line, NULL);
        for (int i = 0; i < sizeof digits; ++i) {
            if (encoded == digits[i]) {
                val = val * 10 + i;
                break;
            }
        }
    }

    return val;
}

int main() {
    const char *filepath = "input.txt";
    FILE *f = fopen(filepath, "r");
    if (f == NULL) err(1, "failed to open file '%s'", filepath);

    char line[256];
    int sum = 0;
    while (fgets(line, sizeof line, f)) sum += parse_line(line);
    printf("%d\n", sum);

    fclose(f);

    return 0;
}
#include <stdio.h>
#if 0
#define FILEPATH "sample.txt"
#else
#define FILEPATH "input.txt"
#endif

#define DAY_NUM 9
unsigned long long sum[DAY_NUM] = {0};
unsigned long long fish_count = 0;

void read_data() {
    FILE *f = fopen(FILEPATH, "r");

    int d;
    while(fscanf(f, "%d,", &d) > 0) {
        sum[d]++;
        fish_count++;
    }

    fclose(f);
}

void print_sum() {
    for (int i = 0; i < DAY_NUM; i++)
        printf("%llu ", sum[i]);
    printf("\ntotal: %llu\n", fish_count);
}

void sim_day() {
    unsigned long long spawn = sum[0];
    for (int j = 1; j < DAY_NUM; j++)
        sum[j-1] = sum[j];
    fish_count+=spawn;
    sum[6]+=spawn;
    sum[8]=spawn;
}

int main() {
    read_data();

    for (int i = 1; i <= 256; i++) {
        sim_day();
        if (i == 80 || i == 256) {
            printf("DAY %d:\n", i);
            print_sum();
        }
    }

    return 0;
}
